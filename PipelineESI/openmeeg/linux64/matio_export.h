
#ifndef MATIO_EXPORT_H
#define MATIO_EXPORT_H

#ifdef MATIO_STATIC_DEFINE
#  define MATIO_EXPORT
#  define MATIO_NO_EXPORT
#else
#  ifndef MATIO_EXPORT
#    ifdef matio_EXPORTS
        /* We are building this library */
#      define MATIO_EXPORT 
#    else
        /* We are using this library */
#      define MATIO_EXPORT 
#    endif
#  endif

#  ifndef MATIO_NO_EXPORT
#    define MATIO_NO_EXPORT 
#  endif
#endif

#ifndef MATIO_DEPRECATED
#  define MATIO_DEPRECATED __attribute__ ((__deprecated__))
#  define MATIO_DEPRECATED_EXPORT MATIO_EXPORT __attribute__ ((__deprecated__))
#  define MATIO_DEPRECATED_NO_EXPORT MATIO_NO_EXPORT __attribute__ ((__deprecated__))
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define MATIO_NO_DEPRECATED
#endif

#endif
