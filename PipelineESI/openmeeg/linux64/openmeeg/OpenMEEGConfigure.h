#ifndef OPENMEEGCONFIGURE_H
#define OPENMEEGCONFIGURE_H

// Here is where system computed values get stored these values should only
// change when the target compile platform changes 
 
/* #undef USE_VTK */

/* #undef USE_GIFTI */

#define HAVE_BLAS

#define HAVE_LAPACK

/* #undef USE_ATLAS */

#define USE_MKL

#define USE_MATIO

/* Define to 1 if your processor stores words with the most significant byte
   first (like Motorola and SPARC, unlike Intel and VAX). */
/* #undef WORDS_BIGENDIAN */

#define HAVE_ISNORMAL_IN_NAMESPACE_STD
/* #undef HAVE_ISNORMAL_IN_MATH_H */

static const char version[] = "2.2.0 (exported)";

#ifdef USE_OMP
#define STATIC_OMP
#else
#define STATIC_OMP static
#endif

#endif  //  ! OPENMEEGCONFIGURE_H
